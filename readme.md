## Source

Tout le projet est basé sur les vidéos de génération procédurale 
proposée par [Brackeys](https://www.youtube.com/c/Brackeys)

- [Generation de mesh](https://www.youtube.com/watch?v=eJEpeUH1EMg)
- [Generation procédurale de 
mesh](https://www.youtube.com/watch?v=64NblGkAabk)
- [Coloration du mesh](https://www.youtube.com/watch?v=lNyZ9K71Vhc)
