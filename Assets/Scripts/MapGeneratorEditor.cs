using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PerlinNoiseMeshGenerator))]
public class MapGeneratorEditor : Editor {
	public override void OnInspectorGUI() {
		PerlinNoiseMeshGenerator meshGenerator = (PerlinNoiseMeshGenerator) target;

		if (DrawDefaultInspector()){
			if (meshGenerator.autoUpdate)
			{
				meshGenerator.CreateMesh();
				meshGenerator.UpdateMesh();		
			}
		}
		if (GUILayout.Button("Generate"))
		{
			meshGenerator.Start();
		}
		
	}
}