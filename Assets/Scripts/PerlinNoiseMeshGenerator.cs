using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class PerlinNoiseMeshGenerator : MonoBehaviour
{

    Mesh mesh;
    MeshCollider meshCollider;

    Vector3[] vertices;
    int[] triangles;

    public int xSize = 160;
    public int zSize = 160;
    public float zoom = .04f;
    public float depth = 20f;

    public Gradient gradient;

    public float xOffset = 500;
    public float zOffset = 500;

    public bool autoUpdate = true;

    Color[] colors;

    float minTerrainHeight;
    float maxTerrainHeight;

    // Start is called before the first frame update
    public void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        xOffset = Random.Range(0f, 99999f) ;
        zOffset = Random.Range(0f, 99999f) ;

        CreateMesh();
        UpdateMesh();
    }

    void Update(){
        // CreateMesh();
        // UpdateMesh();
    }
    
    float getY(int x, int z){
        return Mathf.PerlinNoise(xOffset + x * zoom, zOffset + z * zoom) * depth;
    }

    void updateYMax(float y){
        if (y > maxTerrainHeight)
        {
            maxTerrainHeight=y;
        }
    }
    void updateYMin(float y){
        if (y < minTerrainHeight)
        {
            minTerrainHeight=y;
        }
    }


    public void CreateMesh()
    {
        vertices = new Vector3[(xSize + 1)*(zSize + 1)];
        int i = 0;
        for (int z = 0; z < zSize + 1; z++)
        {
            for (int x = 0; x < xSize + 1; x++)
            {
                float y = getY(x, z);
                vertices[i] = new Vector3(x, y, z);
                updateYMax(y);
                updateYMin(y);

                i++;
            }
        }
        
        int vert = 0;
        int tris = 0;

        triangles = new int[xSize * zSize * 6];
        for (int z = 0; z < zSize; z++)
        {
        
            for (int x = 0; x < xSize; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + xSize + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + xSize + 1;
                triangles[tris + 5] = vert + xSize + 2;

                vert++;
                tris+=6;
            }
            vert++;
        }

        colors = new Color[vertices.Length];

        i = 0;
        for (int z = 0; z < zSize+1; z++)
        {
            for (int x = 0; x < xSize+1; x++)
            {
                float height = Mathf.InverseLerp(minTerrainHeight,maxTerrainHeight,vertices[i].y);
                colors[i] = gradient.Evaluate(height);
                i++;
            }
        }
    }

    public void UpdateMesh()
    {
        mesh.Clear();

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.colors = colors;

        mesh.RecalculateNormals();

        meshCollider = GetComponent<MeshCollider>();
        meshCollider.sharedMesh = mesh; 
    }
}
