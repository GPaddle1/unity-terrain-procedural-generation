using UnityEngine;

public class PerlinNoiseTerrain : MonoBehaviour
{

    public int depth = 20;
    public int width = 500;
    public int height = 500;

    public float scale=10f;

    public float xOffset;
    public float yOffset;

    void Start()
    {
        xOffset=Random.Range(0f, 999999f);
        yOffset=Random.Range(0f, 999999f);
    }

    void Update()
    {
        Terrain terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerateTerrain(terrain.terrainData);
    }

    TerrainData GenerateTerrain(TerrainData terrainData){

        terrainData.heightmapResolution = width + 1;
        terrainData.size = new Vector3(width,depth,height);
        
        terrainData.SetHeights(0,0,GenerateHeights());
        return terrainData;
    }

    void FillColumn(int x, float[,] heights){
        for (var y = 0; y < height; y++)
        {
            heights[x,y] = CalculateHeight(x,y);
        }
    }

    float[,] GenerateHeights(){
        float[,] heights = new float[width,height];
        for (var x = 0; x < width; x++)
        {
            FillColumn(x, heights);            
        }
        return heights;
    }

    float CalculateHeight(int x, int y){
        float xCoord = (float)x / width * scale + xOffset;
        float yCoord = (float)y / height * scale + yOffset;

        float sample = Mathf.PerlinNoise(xCoord, yCoord);
        return sample;
    }
}
